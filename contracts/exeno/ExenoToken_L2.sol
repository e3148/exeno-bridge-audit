// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";
import {ERC165} from "@openzeppelin/contracts/utils/introspection/ERC165.sol";
import {ERC1363} from "erc-payable-token/contracts/token/ERC1363/ERC1363.sol";

interface Mintable {
    function manager() external view returns(address);
    function mint(address, uint256) external;
    function burn(uint256) external;
}

contract ExenoToken_L2 is
    Ownable, ERC165, ERC1363, Mintable
{
    string private constant NAME = "EXENO COIN";
    string private constant SYMBOL = "EXN";

    address public override manager;

    modifier onlyManager() {
        require(msg.sender == manager,
            "Caller is not a manager");
        _;
    }

    constructor()
        ERC20(NAME, SYMBOL)
    {
       manager = msg.sender;
    }

    function setManager(address _manager)
        external onlyOwner
    {
        manager = _manager;
    }

    function mint(address beneficiary, uint256 amount)
        external override onlyManager
    {
        _mint(beneficiary, amount);
    }

    function burn(uint256 amount)
        external override onlyManager
    {
        _burn(msg.sender, amount);
    }

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC165, ERC1363) returns(bool) {
        return interfaceId == type(Mintable).interfaceId || super.supportsInterface(interfaceId);
    }
}
