// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";
import {ERC1363} from "erc-payable-token/contracts/token/ERC1363/ERC1363.sol";

contract ExenoToken_L1 is
    Ownable, ERC1363
{
    string private constant NAME = "EXENO COIN";
    string private constant SYMBOL = "EXN";
    uint256 private constant TOTAL_SUPPLY = 500 * 1000 * 1000 ether;

    constructor(address wallet)
        ERC20(NAME, SYMBOL)
    {
        _mint(wallet, TOTAL_SUPPLY);
    }

    function burn(uint256 amount)
        external onlyOwner
    {
        _burn(msg.sender, amount);
    }
}
