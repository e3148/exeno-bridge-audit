// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import {Math} from "@openzeppelin/contracts/utils/math/Math.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";
import {Pausable} from "@openzeppelin/contracts/security/Pausable.sol";
import {ReentrancyGuard} from "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import {ECDSA} from "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import {IERC1363} from "erc-payable-token/contracts/token/ERC1363/IERC1363.sol";
import {ERC1363Payable} from "erc-payable-token/contracts/payment/ERC1363Payable.sol";

/**
 * This contract facilitates cross-chain bridging of ERC20 tokens (including EXN token) and native currencies.
 * Users (a.k.a. beneficiaries) are expected to make a deposit on one chain (i.e. origin chain) 
 * and then receive a payout in corresponding tokens (or native currency) on another chain (i.e. destination chain),
 * whereas all transfers are coordinated and authorized by Exeno's off-chain cloud infrastructure.
 */

contract ExenoBridgeNode is 
    Ownable,
    Pausable,
    ReentrancyGuard,
    ERC1363Payable
{
    
    // Address where funds are sent when they are extracted from this contract
    address public immutable wallet;
    
    // Address managed by the cloud infrastructure
    address public router;

    // Amount of fees in native currency (a.k.a 'cash') accumulated in this contract
    uint256 public earnedCash;

    // Amount of fees in EXN tokens accumulated in this contract
    uint256 public earnedTokens;

    // Amount of frozen EXN tokens corresponding to the amount of EXN tokens minted on other blockchains
    uint256 public frozenTokens;

    // List of already used nonces for deposits
    mapping(bytes32 => bool) public depositIds;

    // List of already used nonces for withdrawals
    mapping(bytes32 => bool) public withdrawIds;

    // Indicates a deposit has been made - to be captured and acted upon by the cloud infrastructure
    event Deposit(
        bytes32 indexed id,
        bytes32[2] network,
        address[2] beneficiary,
        address[2] token,
        uint256[2] amount,
        uint256[2] feeInCash,
        uint256[2] feeInToken,
        bytes32[2] operation
    );

    // Indicates a withdrawal has been made - only for informational purposes, it's not intended to be monitored by the cloud infrastructure
    event Withdraw(
        bytes32 indexed id,
        bytes32[2] network,
        address[2] beneficiary,
        address[2] token,
        uint256[2] amount,
        uint256[2] feeInCash,
        uint256[2] feeInToken,
        bytes32[2] operation
    );

    // Indicates that the router address has been updated
    event NewRouter(
        address newRouter
    );

    /**
        Data payload describing the entire cross-blockchain transfer
        The first value in a pair refers to what happens on the origin chain, the second value refers to what happens on the destination chain
        operation[0] - allowed values: "burn" | "freeze" | "accept"
        operation[1] - allowed values: "mint" | "unfreeze" | "release"
     */
    struct Data {
        bytes32 id;
        bytes32[2] network;
        address[2] beneficiary;
        address[2] token;
        uint256[2] amount;
        uint256[2] feeInCash;
        uint256[2] feeInToken;
        bytes32[2] operation;
    }

    modifier onlyRouter() {
        require(msg.sender == router,
            "ExenoBridgeNode: unauthorized router");
        _;
    }

    modifier validAddress(address a) {
        require(a != address(0),
            "ExenoBridgeNode: address cannot be zero");
        require(a != address(this),
            "ExenoBridgeNode: invalid address");
        _;
    }

    constructor(
        address _token,
        address _wallet,
        address _router
    )
        validAddress(_token)
        validAddress(_wallet)
        validAddress(_router)
        ERC1363Payable(IERC1363(_token))
    {
        wallet = _wallet;
        router = _router;
    }

    /**
     * Gateway for deposits to be executed by the beneficiary - with arguments listed explicitly
     * @param fulfilled - flag that needs to be set to `false` indicating it's a deposit
     * @param timeout - timestamp indicating the latest time a deposit is allowed
     * @param signature - all arguments need to be signed by the router, indicating the router's acceptance for the deposit's execution
     */
    function deposit(
        bytes32 id,
        bytes32[2] calldata network,
        address[2] calldata beneficiary,
        address[2] calldata token,
        uint256[2] calldata amount,
        uint256[2] calldata feeInCash,
        uint256[2] calldata feeInToken,
        bytes32[2] calldata operation,
        bool fulfilled,
        uint32 timeout,
        bytes calldata signature
    )
        external payable whenNotPaused
    {
        Data memory data = Data(id, network, beneficiary, token, amount, feeInCash, feeInToken, operation);
        _verifySignature(data, fulfilled, timeout, signature);
        _validateDeposit(data, fulfilled);
        _validateDeposit(data, timeout);
        _processDeposit(data);
    }

    /**
     * Gateway for deposits to be executed by the beneficiary - with arguments packed into a single encoding
     */
    function deposit(
        bytes calldata encodedData
    )
        external payable whenNotPaused
    {
        (Data memory data, bool fulfilled, uint32 timeout, bytes memory signature) = _unpackSignedData(encodedData);
        _verifySignature(data, fulfilled, timeout, signature);
        _validateDeposit(data, fulfilled);
        _validateDeposit(data, timeout);
        _processDeposit(data);
    }

    /**
     * Gateway for withdrawals to be executed by the beneficiary - with arguments listed explicitly
     * @param fulfilled - flag that needs to be set to `true` indicating it's a withdrawal
     * @param timeout - timestamp indicating the earliest time a withdrawal is allowed
     * @param signature - all arguments need to be signed by the router, indicating the router's acceptance for the withdrawal's execution
     */
    function withdraw(
        bytes32 id,
        bytes32[2] calldata network,
        address[2] calldata beneficiary,
        address[2] calldata token,
        uint256[2] calldata amount,
        uint256[2] calldata feeInCash,
        uint256[2] calldata feeInToken,
        bytes32[2] calldata operation,
        bool fulfilled,
        uint32 timeout,
        bytes calldata signature
    )
        external payable whenNotPaused
    {
        Data memory data = Data(id, network, beneficiary, token, amount, feeInCash, feeInToken, operation);
        _verifySignature(data, fulfilled, timeout, signature);
        _validateWithdraw(data, fulfilled);
        _validateWithdraw(data, timeout);
        _processWithdraw(data);
    }

    /**
     * Gateway for withdrawals to be executed by the beneficiary - with arguments packed into a single encoding
     */
    function withdraw(
        bytes calldata encodedData
    )
        external payable whenNotPaused
    {
        (Data memory data, bool fulfilled, uint32 timeout, bytes memory signature) = _unpackSignedData(encodedData);
        _verifySignature(data, fulfilled, timeout, signature);
        _validateWithdraw(data, fulfilled);
        _validateWithdraw(data, timeout);
        _processWithdraw(data);
    }

    /**
     * Dry-run probe for a deposit payload
     */
    function depositDryRun(
        bytes calldata encodedData
    )
        external view returns(bool)
    {
        (Data memory data, bool fulfilled, uint32 timeout, bytes memory signature) = _unpackSignedData(encodedData);
        _verifySignature(data, fulfilled, timeout, signature);
        require(!fulfilled,
            "ExenoBridgeNode: this payload is not a deposit");
        require(_isNative(data.token[0]) || IERC20(data.token[0]).allowance(data.beneficiary[0], address(this)) >= data.amount[0],
            "ExenoBridgeNode: before depositing user must approve amount");
        require(!depositIds[data.id],
            "ExenoBridgeNode: deposit already processed");
        if (_isMintable(data.token[0])) {
            require(data.operation[0] == bytes32("burn"),
                "ExenoBridgeNode: burn operation is expected for a mintable token");
        } else if (data.operation[0] == bytes32("freeze")) {
            require(_isCore(data.token[0]),
                "ExenoBridgeNode: freeze operation cannot be done on a non-core token");
        } else {
            require(data.operation[0] == bytes32("accept"),
                "ExenoBridgeNode: unexpected deposit operation");
        }
        return timeout == 0 || block.timestamp <= timeout;
    }

    /**
     * Dry-run probe for a withdrawal payload
     */
    function withdrawDryRun(
        bytes calldata encodedData
    )
        external view returns(bool)
    {
        (Data memory data, bool fulfilled, uint32 timeout, bytes memory signature) = _unpackSignedData(encodedData);
        _verifySignature(data, fulfilled, timeout, signature);
        require(fulfilled,
            "ExenoBridgeNode: this payload is not a withdraw");
        require(!withdrawIds[data.id],
            "ExenoBridgeNode: withdraw already processed");
        if (_isMintable(data.token[1])) {
            require(data.operation[1] == bytes32("mint"),
                "ExenoBridgeNode: mint operation is expected for a mintable token");
        } else if (data.operation[1] == bytes32("unfreeze")) {
            require(_isCore(data.token[1]),
                "ExenoBridgeNode: unfreeze operation cannot be done on a non-core token");
            require(frozenTokens >= data.amount[1],
                "ExenoBridgeNode: cannot unfreeze as there are not enough frozen tokens");
            require(ownedTokens(data.token[1]) >= data.amount[1],
                "ExenoBridgeNode: cannot unfreeze as there are not enough owned tokens");
        } else {
            require(data.operation[1] == bytes32("release"),
                "ExenoBridgeNode: unexpected withdraw operation");
            if (_isNative(data.token[1])) {
                require(availableCash() >= data.amount[1],
                    "ExenoBridgeNode: cannot release cash as there is not enough cash available");
            } else {
                require(availableTokens(data.token[1]) >= data.amount[1],
                    "ExenoBridgeNode: cannot release tokens as there are not enough tokens available");
            }
        }
        return timeout == 0 || block.timestamp >= timeout;
    }

    /**
     * Gateway for payouts executed by the router - with arguments listed explicitly
     */
    function payout(
        bytes32 id,
        bytes32[2] calldata network,
        address[2] calldata beneficiary,
        address[2] calldata token,
        uint256[2] calldata amount,
        uint256[2] calldata feeInCash,
        uint256[2] calldata feeInToken,
        bytes32[2] calldata operation
    )
        external onlyRouter
    {
        _processWithdraw(Data(id, network, beneficiary, token, amount, feeInCash, feeInToken, operation));
    }

    /**
     * Gateway for payouts to be executed by the router - with arguments packed into a single encoding
     */
    function payout(
        bytes calldata encodedData
    )
        external onlyRouter
    {
        _processWithdraw(_unpackData(encodedData));
    }

    /**
     * We want the contract to be able to receive native currency - it's needed when we want to add liquidity to it
     */
    receive()
        external payable
    {}

    /**
     * Implementation of ERC1363Payable
     * When EXN tokens are sent to this contract via `transferAndCall` we interprete it as an act of depositing or withdrawing
     * @param sender The address performing the action
     * @param amount The amount of tokens transferred
     * @param encodedData Encoded payload which specifies the intended deposit or withdrawal
     */
    function _transferReceived(
        address,
        address sender,
        uint256 amount,
        bytes memory encodedData
    )
        internal override whenNotPaused
    {
        (Data memory data, bool fulfilled, uint32 timeout, bytes memory signature) = _unpackSignedData(encodedData);
        _verifySignature(data, fulfilled, timeout, signature);
        if (!fulfilled) { // in case it's a deposit we treat tokens sent to this contract as a deposit and/or fee payment    
            _validateDeposit(data, sender, amount);
            _validateDeposit(data, timeout);
            _processDeposit(data);
        } else { // in case it's a withdrawal we treat tokens sent to this contract as a fee payment
            _validateWithdraw(data, sender, amount);
            _validateWithdraw(data, timeout);
            _processWithdraw(data);
        }
    }

    /**
     * Only payloads signed by the router should be accepted
     * Make sure that arguments contained in the payload match the arguments encoded in the signed message
     */
    function _verifySignature(
        Data memory d,
        bool fulfilled,
        uint32 timeout,
        bytes memory signature
    )
        internal view
    {
        bytes memory payload = abi.encodePacked(d.id, d.network, d.beneficiary, d.token, d.amount, d.feeInCash, d.feeInToken, d.operation, fulfilled, timeout);
        address signer = ECDSA.recover(ECDSA.toEthSignedMessageHash(keccak256(payload)), signature);
        require(signer == router,
            "ExenoBridgeNode: unauthorized signer");
    }

    /**
     * When fee is paid in native currency (a.k.a 'cash'), validate the following aspects of a deposit:
     * (a) the `fulfilled` flag is set to `false`
     * (b) the sender matches the beneficiary
     * (c) the attached payment and/or allowance match the values defined in the payload
     */
    function _validateDeposit(
        Data memory d,
        bool fulfilled
    )
        internal nonReentrant
    {
        require(!fulfilled,
            "ExenoBridgeNode: this payload is not a deposit");
        require(msg.sender == d.beneficiary[0],
            "ExenoBridgeNode: unexpected sender");
        if (_isNative(d.token[0])) {
            require(msg.value >= d.amount[0] + d.feeInCash[0],
                "ExenoBridgeNode: not enough cash attached to cover amount and fee");
        } else {
            require(msg.value >= d.feeInCash[0],
                "ExenoBridgeNode: not enough cash attached to cover fee");
            require(IERC20(d.token[0]).allowance(msg.sender, address(this)) >= d.amount[0],
                "ExenoBridgeNode: before depositing user must approve amount");
            require(IERC20(d.token[0]).transferFrom(msg.sender, address(this), d.amount[0]),
                "ExenoBridgeNode: ERC20 transferFrom failed");
        }
        earnedCash += d.feeInCash[0];
    }

    /**
     * When fee is paid in core tokens, validate the following aspects of a deposit:
     * (a) the sender matches the beneficiary
     * (b) the transferred amount and/or allowance match the values defined in the payload
     */
    function _validateDeposit(
        Data memory d,
        address sender,
        uint256 amount
    )
        internal nonReentrant
    {
        require(sender == d.beneficiary[0],
            "ExenoBridgeNode: unexpected sender");
        if (_isCore(d.token[0])) {
            require(amount >= d.amount[0] + d.feeInToken[0],
                "ExenoBridgeNode: not enough core tokens transferred to cover amount and fee");
        } else {
            require(d.feeInToken[0] > 0,
                "ExenoBridgeNode: fee needs to be non-zero");
            require(amount >= d.feeInToken[0],
                "ExenoBridgeNode: not enough core tokens transferred to cover fee");
            require(IERC20(d.token[0]).allowance(sender, address(this)) >= d.amount[0],
                "ExenoBridgeNode: before depositing user must approve amount");
            require(IERC20(d.token[0]).transferFrom(sender, address(this), d.amount[0]),
                "ExenoBridgeNode: ERC20 transferFrom failed");
        }
        earnedTokens += d.feeInToken[0];
    }

    /**
     * Validate the following aspects of a deposit:
     * (a) the time-window for the deposit has not expired yet
     * (b) the deposit has not been processed yet
     */
    function _validateDeposit(
        Data memory d,
        uint32 timeout
    )
        internal
    {
        require(timeout == 0 || block.timestamp <= timeout,
            "ExenoBridgeNode: time-window for deposit has already expired");
        require(!depositIds[d.id],
            "ExenoBridgeNode: deposit already processed");
        depositIds[d.id] = true;
    }

    /**
     * When fee is paid in native currency (a.k.a 'cash'), validate the following aspects of a withdrawal:
     * (a) the `fulfilled` flag is set to `true`
     * (b) the attached payment matches the value defined in the payload
     */
    function _validateWithdraw(
        Data memory d,
        bool fulfilled
    )
        internal
    {
        require(fulfilled,
            "ExenoBridgeNode: this payload is not a withdraw");
        require(msg.value >= d.feeInCash[1],
            "ExenoBridgeNode: not enough cash attached to cover fee");
        earnedCash += d.feeInCash[1];
    }

    /**
     * When fee is paid in core tokens, validate the following aspects of a withdrawal:
     * (a) the sender matches the beneficiary 
     * (b) the transferred amount matches the value defined in the payload
     */
    function _validateWithdraw(
        Data memory d,
        address sender,
        uint256 amount
    )
        internal
    {
        require(sender == d.beneficiary[1],
            "ExenoBridgeNode: unexpected sender");
        require(d.feeInToken[1] > 0,
            "ExenoBridgeNode: token fee needs to be non-zero");
        require(amount >= d.feeInToken[1],
            "ExenoBridgeNode: not enough core tokens transferred to cover fee");
        earnedTokens += d.feeInToken[1];
    }

    /**
     * Validate the following aspects of a withdrawal:
     * (a) the time-window for the withdrawal has already started
     * (b) the withdrawal has not been processed yet
     */
    function _validateWithdraw(
        Data memory d,
        uint32 timeout
    )
        internal
    {
        require(timeout == 0 || block.timestamp >= timeout,
            "ExenoBridgeNode: time-window for withdraw has not started yet");
        require(!withdrawIds[d.id],
            "ExenoBridgeNode: withdraw already processed");
        withdrawIds[d.id] = true;
    }

    /**
     * Process a deposit according to the instruction defined in the `operation[0]` parameter
     * There are 3 options: 
     * (a) "burn" - in case the token on the origin chain is mintable
     * (b) "freeze" - in case the token on the destination chain is mintable
     * (c) "accept" - in all other cases
     */
    function _processDeposit(Data memory d)
        internal nonReentrant
    {
        if (_isMintable(d.token[0])) {
            require(d.operation[0] == bytes32("burn"),
                "ExenoBridgeNode: burn operation is expected for a mintable token");
            Mintable(d.token[0]).burn(d.amount[0]);
        } else if (d.operation[0] == bytes32("freeze")) {
            require(_isCore(d.token[0]),
                "ExenoBridgeNode: freeze operation cannot be done on a non-core token");
            frozenTokens += d.amount[0];
        } else {
            require(d.operation[0] == bytes32("accept"),
                "ExenoBridgeNode: unexpected deposit operation");
        }
        emit Deposit(d.id, d.network, d.beneficiary, d.token, d.amount, d.feeInCash, d.feeInToken, d.operation);
    }

    /**
     * Process a withdrawal according to the instruction defined in the `operation[1]` parameter
     * There are 3 options: 
     * (a) "mint" - in case the token on the destination chain is mintable
     * (b) "unfreeze" - in case the token on the origin chain is mintable
     * (c) "release" - in all other cases
     */
    function _processWithdraw(Data memory d)
        internal nonReentrant
    {
        if (_isMintable(d.token[1])) {
            require(d.operation[1] == bytes32("mint"),
                "ExenoBridgeNode: mint operation is expected for a mintable token");
            Mintable(d.token[1]).mint(d.beneficiary[1], d.amount[1]);
        } else if (d.operation[1] == bytes32("unfreeze")) {
            require(_isCore(d.token[1]),
                "ExenoBridgeNode: unfreeze operation cannot be done on a non-core token");
            require(frozenTokens >= d.amount[1],
                "ExenoBridgeNode: cannot unfreeze as there are not enough frozen tokens");
            require(ownedTokens(d.token[1]) >= d.amount[1],
                "ExenoBridgeNode: cannot unfreeze as there are not enough owned tokens");
            require(IERC20(d.token[1]).transfer(d.beneficiary[1], d.amount[1]),
                "ExenoBridgeNode: ERC20 transfer failed");
            frozenTokens -= d.amount[1];
        } else {
            require(d.operation[1] == bytes32("release"),
                "ExenoBridgeNode: unexpected withdraw operation");
            if (_isNative(d.token[1])) {
                require(availableCash() >= d.amount[1],
                    "ExenoBridgeNode: cannot release cash as there is not enough cash available");
                Address.sendValue(payable(d.beneficiary[1]), d.amount[1]);
            } else {
                require(availableTokens(d.token[1]) >= d.amount[1],
                    "ExenoBridgeNode: cannot release tokens as there are not enough tokens available");
                require(IERC20(d.token[1]).transfer(d.beneficiary[1], d.amount[1]),
                    "ExenoBridgeNode: ERC20 transfer failed");
            }
        }
        emit Withdraw(d.id, d.network, d.beneficiary, d.token, d.amount, d.feeInCash, d.feeInToken, d.operation);
    }

    /**
     * Helper function for unpacking encoded payloads of unsigned `Data`
     */
    function _unpackData(bytes memory encodedData)
        internal pure returns(Data memory)
    {
        (
            bytes32 id,
            bytes32[2] memory network,
            address[2] memory beneficiary,
            address[2] memory token,
            uint256[2] memory amount,
            uint256[2] memory feeInCash,
            uint256[2] memory feeInToken,
            bytes32[2] memory operation
        ) = abi.decode(encodedData, (bytes32, bytes32[2], address[2], address[2],  uint256[2], uint256[2], uint256[2], bytes32[2]));
        return Data(id, network, beneficiary, token, amount, feeInCash, feeInToken, operation);
    }

    /**
     * Helper function for unpacking encoded payloads of signed `Data` 
     */
    function _unpackSignedData(bytes memory encodedData)
        internal pure returns(Data memory, bool, uint32, bytes memory)
    {
        (
            bytes32 id,
            bytes32[2] memory network,
            address[2] memory beneficiary,
            address[2] memory token,
            uint256[2] memory amount,
            uint256[2] memory feeInCash,
            uint256[2] memory feeInToken,
            bytes32[2] memory operation,
            bool fulfilled,
            uint32 timeout,
            bytes memory signature
        ) = abi.decode(encodedData, (bytes32, bytes32[2], address[2], address[2], uint256[2], uint256[2], uint256[2], bytes32[2], bool, uint32, bytes));
        return (Data(id, network, beneficiary, token, amount, feeInCash, feeInToken, operation), fulfilled, timeout, signature);
    }

    /**
     * Returns the IERC1363 token associated with this contract, i.e. EXN token
     */
    function _coreToken()
        internal view returns(address)
    {
        return address(acceptedToken());
    }

    /**
     * Checks whether we are dealing with the core token (i.e. EXN token)
     */
    function _isCore(address token)
        internal view returns(bool)
    {
        return token == _coreToken();
    }

    /**
     * Checks whether we are dealing with the native currency (indicated as zero address)
     */
    function _isNative(address token)
        internal pure returns(bool)
    {
        return token == address(0);
    }

    /**
     * Checks whether we are dealing with a mintable core token (not all incarnations of the EXN token are mintable)
     */
    function _isMintable(address token)
        internal view returns(bool)
    {
        if (!_isCore(token)) {
            return false;
        }
        Mintable mintable = Mintable(token);
        try mintable.manager() returns(address manager) {
            return manager == address(this);
        } catch (bytes memory) {
            return false;
        }
    }

    /**
     * Current balance of native currency
     */
    function ownedCash()
        public view returns(uint256)
    {
        return address(this).balance;
    }

    /**
     * Current balance of the core token (i.e. EXN token)
     */
    function ownedTokens()
        external view returns(uint256)
    {
        return ownedTokens(_coreToken());
    }

    /**
     * Current balance of any ERC20 token
     */
    function ownedTokens(address token)
        public view returns(uint256)
    {
        return IERC20(token).balanceOf(address(this));
    }

    /**
     * How much native currency is available for withdrawals/payouts
     */
    function availableCash()
        public view returns(uint256)
    {
        return ownedCash();
    }

    /**
     * How many core tokens (i.e. EXN tokens) are available for withdrawals/payouts
     */
    function availableTokens()
        external view returns(uint256)
    {
        return availableTokens(_coreToken());
    }

    /**
     * How many ERC20 tokens are available for withdrawals/payouts
     */
    function availableTokens(address token)
        public view returns(uint256)
    {
        // Frozen tokens are not meant to be part of the liquidity pool
        // They should only be released as a result of burning of EXN tokens on some other chain
        if (_isCore(token)) {
            return Math.max(ownedTokens(_coreToken()) - frozenTokens, 0);
        }
        return ownedTokens(token);
    }

    /**
     * Release all available funds in native currency
     */
    function releaseAvailableCash()
        external onlyOwner
    {
        uint256 balance = availableCash();
        require(balance > 0,
            "ExenoBridgeNode: there is no available cash to release");
        releaseAvailableCash(balance);
    }

    /**
     * Release a specified amount of available funds in native currency
     */
    function releaseAvailableCash(uint256 amount)
        public onlyOwner
    {
        require(amount <= availableCash(),
            "ExenoBridgeNode: amount to release is more than available");
        Address.sendValue(payable(wallet), amount);
    }

    /**
     * Release all available funds in the core token (i.e. EXN token)
     */
    function releaseAvailableTokens()
        external onlyOwner
    {
        releaseAvailableTokens(_coreToken());
    }

    /**
     * Release a specified amount of the core token (i.e. EXN token)
     */
    function releaseAvailableTokens(uint256 amount)
        external onlyOwner
    {
        releaseAvailableTokens(_coreToken(), amount);
    }

    /**
     * Release all available funds in an ERC20 token
     */
    function releaseAvailableTokens(address token)
        public onlyOwner
    {
        uint256 balance = availableTokens(token);
        require(balance > 0,
            "ExenoBridgeNode: there are no available tokens to release");
       releaseAvailableTokens(token, balance);
    }

    /**
     * Release a specified amount of an ERC20 token
     */
    function releaseAvailableTokens(
        address token,
        uint256 amount
    )
        public onlyOwner nonReentrant
    {
        require(amount <= availableTokens(token),
            "ExenoBridgeNode: amount to release is more than available");
        require(IERC20(token).transfer(wallet, amount),
            "ExenoBridgeNode: ERC20 transfer failed");
    }

    /**
     * Change the router - only to be used by the owner
     */
    function setRouter(address newRouter)
        external onlyOwner validAddress(newRouter)
    {
        router = newRouter;
        emit NewRouter(newRouter);
    }

    /**
     * Pause all deposit and withdrawal operations
     */
    function pause()
        external onlyOwner
    {
        _pause();
    }
    
    /**
     * Unpause all deposit and withdrawal operations
     */
    function unpause()
        external onlyOwner
    {
        _unpause();
    }
}

/**
 * Implemented by every mintable incarnation of the EXN token
 */
interface Mintable {
    function manager() external view returns(address);
    function mint(address, uint256) external;
    function burn(uint256) external;
}