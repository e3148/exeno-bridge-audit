const Web3 = require("web3");
const Accounts = require("web3-eth-accounts");
const AbiCoder = require('web3-eth-abi');
const { isBN, toWei } = require("web3-utils");

export const toTimestamp = (d) => {
    return (typeof d === "number") ? d : Math.round(Date.parse(d) / 1000);
}

export const toHex = (s) => {
    return Web3.utils.asciiToHex(s).toString().padEnd(66, "0");
}

export const toHexPair = (pair) => {
    return [toHex(pair[0]), toHex(pair[1])];
}

export const toNum = (s) => {
    return typeof s === "string" ? s : isBN(s) ? s.toString() : toWei(s.toString());
}

export const toNumPair = (pair) => {
    return [toNum(pair[0]), toNum(pair[1])];
}

export const sign = (input, id, fulfilled, timeout, privateKey) => {
    const data = {
        id: id ? id : Web3.utils.randomHex(32),
        network: toHexPair(input.network),
        beneficiary: input.beneficiary,
        token: input.token,
        amount: toNumPair(input.amount),
        feeInCash: toNumPair(input.feeInCash),
        feeInToken: toNumPair(input.feeInToken),
        operation: toHexPair(input.operation)
    }
    const encoding = AbiCoder.encodeParameters(
        ["bytes32", "bytes32[2]", "address[2]", "address[2]", "uint256[2]", "uint256[2]", "uint256[2]", "bytes32[2]"], 
        [data.id, data.network, data.beneficiary, data.token, data.amount, data.feeInCash, data.feeInToken, data.operation]
    );
    const signableData = { ... data, fulfilled, timeout: toTimestamp(timeout) };
    const message = Web3.utils.soliditySha3(
        { t: "bytes32", v: signableData.id },
        { t: "bytes32[2]", v: signableData.network },
        { t: "address[2]", v: signableData.beneficiary },
        { t: "address[2]", v: signableData.token },
        { t: "uint256[2]", v: signableData.amount },
        { t: "uint256[2]", v: signableData.feeInCash },
        { t: "uint256[2]", v: signableData.feeInToken },
        { t: "bytes32[2]", v: signableData.operation },
        { t: "bool", v: signableData.fulfilled },
        { t: "uint32", v: signableData.timeout }
    ).toString("hex");
    const accounts = new Accounts();
    const { signature } = accounts.sign(message, privateKey ? privateKey : process.env.PRIVATE_KEY);
    const signedData = { ... signableData, message, signature };
    const signedEncoding = AbiCoder.encodeParameters(
        ["bytes32", "bytes32[2]", "address[2]", "address[2]", "uint256[2]", "uint256[2]", "uint256[2]", "bytes32[2]", "bool", "uint32", "bytes"], 
        [signedData.id, signedData.network, signedData.beneficiary, signedData.token, signedData.amount, signedData.feeInCash, signedData.feeInToken, signedData.operation, signedData.fulfilled, signedData.timeout, signedData.signature]
    );
    return { data, encoding, signedData, signedEncoding };
}
