# Exeno Bridge Node Contract

## Purpose

The main purpose of the contract is to facilitate cross-chain bridging of ERC20 tokens (including EXN token) and native currencies. This is achieved by enabling users (a.k.a. beneficiaries) to make a deposit on one chain (i.e. origin chain) and then withdraw (or receive) corresponding tokens (or native currency) on another chain (i.e. destination chain), whereas all operations on both chains are coordinated and authorized by Exeno's Off-Chain Cloud Infrastructure (EOCCI). In the contract's codebase, EOCCI is referred to as the "router".

## Workflow

1. The user shares with EOCCI his/her intention to perform a cross-chain transfer by specifying the asset to be transferred, its amount and the destination chain.

2. EOCCI determines the appropriate asset on the destination chain and the bridging fee, and then approves the transfer by issuing a signed payload with all details needed for the cross-chain transfer.

3. On the origin chain, the signed payload is used by the user to deposit funds to the contract.

4. On the origin chain, the contract emits a deposit event which is captured by EOCCI.

5. On the destination chain, funds are released to the user by applying one of the following options: 
   (a) The user (or any other party) invokes the "withdraw" method using the signed payload.
   (b) EOCCI invokes the "payout" method using an unsigned payload.

## Features

* The contract supports bridging for the following types of assets:
  - EXN tokens
  - any other ERC20 tokens
  - blockchain's native currency (e.g. ETH, MATIC, BNB etc)
* There is only one EOCCI but there are multiple instances of the contract deployed on different EVM-based blockchains (exactly one instance per blockchain).
* Both deposits and withdrawals can only be made when the user gets authorization from EOCCI. This authorization is realized in the form of transaction payload being signed by EOCCI. The validity of this signature is verified by the contract.
* The contract ensures that the same deposit or withdrawal cannot be made more than once. Thus, the ID of a deposit or withdrawal is expected to be unique, while a deposit and the corresponding withdrawal can share the same ID.
* A deposit always needs to be made by the user (a.k.a. beneficiary). However, in case of withdrawals, as an alternative to the user making it himself/herself, EOCCI can choose to make it for the user by invoking the "payout" method. Whatever option is chosen, the contract ensures that there can be either a withdrawal (by the user) or a payout (by EOCCI), but not both.
* The contract implements the [ERC1363Payable](https://vittominacori.github.io/erc1363-payable-token/#erc1363payable) interface. When EXN tokens are sent to the contract this action is interpreted as an act of depositing or withdrawing, depending on the content of the attached payload.
* When a deposit or a withdrawal (or a payout) is made, the contract emits the appropriate event containing all details of the intended cross-chain transfer:
  - The "Deposit" event indicates that a deposit has been made. It's intended to captured and acted upon by EOCCI.
  - The "Withdraw" event indicates that a withdrawal (or a payout) has been made. It's only for informational purposes - it's not intended to be monitored by EOCCI.
* When making a deposit or a withdrawal, the user might be required to make an additional payment to cover the bridging fee. This fee can be paid either in the native currency or EXN tokens.
* Both deposits and withdrawals are restricted in time, i.e. the contract ensures that a deposit cannot be made after a specified point in time, and a withdrawal cannot be made before a specified point in time. Payouts executed by EOCCI are not restricted in time.
* Depending on the type of asset being deposited and withdrawn (or paid out), the contract follows EOCCI instructions contained in the payload and applies one of the following types of operations:
  - For deposits:
       (a) "burn" - in case the token on the origin chain is mintable
       (b) "freeze" - in case the token on the destination chain is mintable
       (c) "accept" - in all other cases
  - For withdrawals:
       (a) "mint" - in case the token on the destination chain is mintable
       (b) "unfreeze" - in case the token on the origin chain is mintable
       (c) "release" - in all other cases
* EOCCI has the business logic to determine the appropriate fees and operation types, on both origin and destination chains for a given cross-blockchain transfer. The contract cannot do it itself, as it has no access to other blockchains. However, the contract ensures that the type of operation imposed by EOCCI is consistent with the local setup, e.g. "mint" operation cannot be executed on a non-mintable token etc.
* In order to mange the contract's liquidity in different assets, anyone is able to add funds to the contract, i.e. transfer any asset (i.e. ERC20 tokens or native currency) to the contract's address. However, only the contract's owner has the right to remove funds from the contract - any funds except for frozen EXN tokens.
* Frozen EXN tokens can only be released by EOCCI and it's done by invoking the "payout" method. This procedure should only be applied when the entire contract needs to be decommissioned and there is no other way to extract the frozen EXN tokens.
* The EOCCI address (i.e. the router) can only be changed by the contract's owner. A new owner address can be assigned only by the existing owner. Also, the owner is the only entity allowed to renounce ownership - after this is done the contract is no longer owned by anyone.
* The contract can be paused (and then unpaused) at any time by its owner. When the contract is paused, no deposits or withdrawals are possible, including transactions initiated through the ERC1363Payable interface. Payouts are not subject to pausing.

## Scope

The contract's primary use-case is cross-blockchain transfers. However, the contract is designed with a broader perspective in mind, so that in the future the same codebase will also be able to accommodate the following use-cases:
- Cross-currency swaps (within one blockchain or cross-chain)
- Escrow transactions between two different users (with EOCCI acting as an arbitrator)

## Compile

**Note**: for compilation to be successful, EVM optimizer needs to be turned on (and set to 200 runs).

Here are the steps needed to compile the contract's code:

1. Install dependencies:
   ```
   npm install
   ```
2. And then run:

   ```
   truffle compile
   ```

## Test

Unit test cases are located [here](https://gitlab.com/e3148/exeno-bridge-audit/-/tree/main/test) but they are not complete yet.

**Note**: the following simplifications are applied in unit testing:

* As cross-blockchain testing is not possible with a standard testing framework, unit testing is performed with multiple ERC20 tokens deployed on the same local blockchain.
* In the production environment payload signing is supposed to be executed by EOCCI (i.e. the router), however for unit testing purposes those actions are simulated by a JavaScript module located [here](https://gitlab.com/e3148/exeno-bridge-audit/-/blob/main/utils/router.js).

Here are the steps needed to run unit tests:

1. Store the router's private key in an environment variable named `PRIVATE_KEY`, e.g.:
   ```
   $env:PRIVATE_KEY = "b5af4eb4d0..8d0e4027b2fe"
   ```
   ```
   export PRIVATE_KEY = "b5af4eb4d0..8d0e4027b2fe"
   ```
   
3. And then run:
   ```
   truffle test
   ```