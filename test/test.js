const { constants, expectRevert, expectEvent } = require('@openzeppelin/test-helpers');
const { toWei, fromWei, toBN, randomHex } = require('web3-utils');
const { sign } = require('../utils/router');
const { advanceTimeAndBlock, getBlock, period } = require("../utils/time");

require('chai').use(require('chai-as-promised')).should();

const ExenoToken_L1 = artifacts.require('ExenoToken_L1');
const ExenoToken_L2 = artifacts.require('ExenoToken_L2');
const ExenoBridgeNode = artifacts.require('ExenoBridgeNode');

const n = (s) => { return toBN(toWei(s)) }

contract('ExenoBridgeNode', (accounts) => {
    
    const owner = accounts[0];
    const wallet = accounts[1];
    const beneficiary = accounts[5];
    const beneficiary2 = accounts[6];
    const router = accounts[8];

    const domainA = 'domainA';
    const domainB = 'domainB';
    const domainX = 'domainX';
    const domainY = 'domainY';

    let tokenA;
    let tokenB;
    let tokenX;
    let tokenY;
    let token1;
    let token2;
    
    let nodeA;
    let nodeB;
    let nodeX;
    let nodeY;

    let initialAmountTokenA;
    let initialAmountTokenB;
    let initialAmountTokenX;
    let initialAmountTokenY;
    let initialAmountToken1;
    let initialAmountToken2;
    let initialAmountCash;

    let input;
    let block;
    
    beforeEach(async () => {
        tokenA = await ExenoToken_L1.new(owner);
        tokenB = await ExenoToken_L1.new(owner);
        tokenX = await ExenoToken_L2.new();
        tokenY = await ExenoToken_L2.new();

        nodeA = await ExenoBridgeNode.new(tokenA.address, wallet, router);
        nodeB = await ExenoBridgeNode.new(tokenB.address, wallet, router);
        nodeX = await ExenoBridgeNode.new(tokenX.address, wallet, router);
        nodeY = await ExenoBridgeNode.new(tokenY.address, wallet, router);

        await tokenX.methods['setManager(address)'](nodeX.address);
        await tokenY.methods['setManager(address)'](nodeY.address);
    });

    describe('Happy path', function () {
        describe('Plain vanilla', function () {

            beforeEach(async () => {
                initialAmountTokenA = n('1974');
                await tokenA.methods['transfer(address,uint256)'](beneficiary, initialAmountTokenA);
                input = {
                    network: [domainA, domainX],
                    beneficiary: [beneficiary, beneficiary],
                    token: [tokenA.address, tokenX.address],
                    amount: [n('255.89'), n('255.89')],
                    feeInCash: [n('0.0000044'), n('0.0000088')],
                    feeInToken: [n('1.20'), n('0.65')],
                    operation: ['freeze', 'mint']
                }
            });

            it('deposit | payout', async function () {

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA);
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal('0');

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });
                    
                    const receipt = await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.feeInCash[0], from: beneficiary });

                    console.log('nodeA.deposit():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal(input.amount[0]);
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[0]);
                (await nodeA.methods['earnedTokens()']()).should.be.bignumber.equal('0');
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA.sub(input.amount[0]));
                
                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    const receipt = await nodeX.methods['payout(bytes)'](
                        encoding, { from: router });

                    console.log('nodeX.payout():\t\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeX, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedTokens()']()).should.be.bignumber.equal('0');
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(input.amount[1]);
            });

            it('deposit | withdraw', async function () {
                
                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA);
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal('0');

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });
                    
                    const receipt = await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.feeInCash[0], from: beneficiary });

                    console.log('nodeA.deposit():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal(input.amount[0]);
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[0]);
                (await nodeA.methods['earnedTokens()']()).should.be.bignumber.equal('0');
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA.sub(input.amount[0]));

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    const receipt = await nodeX.methods['withdraw(bytes)'](
                        signedEncoding, { value: input.feeInCash[1], from: beneficiary });

                    console.log('nodeX.withdraw():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeX, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[1]);
                (await nodeX.methods['earnedTokens()']()).should.be.bignumber.equal('0');
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(input.amount[1]);
            });

            it('transfer | payout | transfer | transfer', async function () {

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA);
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal('0');

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    const receipt = await tokenA.methods['transferAndCall(address,uint256,bytes)'](
                        nodeA.address, input.amount[0].add(input.feeInToken[0]), signedEncoding, { from: beneficiary });

                    console.log('tokenA.transferAndCall():\t', receipt.tx);
                    
                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal(input.amount[0]);
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal('0');
                (await nodeA.methods['earnedTokens()']()).should.be.bignumber.equal(input.feeInToken[0]);
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA.sub(input.amount[0]).sub(input.feeInToken[0]));
                
                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);            
                    
                    const receipt = await nodeX.methods['payout(bytes)'](
                        encoding, { from: router });

                    console.log('nodeX.payout():\t\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeX, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedTokens()']()).should.be.bignumber.equal('0');
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(input.amount[1]);
                
                {
                    const id = '0x6ea622eae6590570ec3fd954d787638ea6b2ea0c7dd939dcdd04d8b824ff911f';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    const receipt = await tokenA.methods['transferAndCall(address,uint256,bytes)'](
                        nodeA.address, input.amount[0].add(input.feeInToken[0]), signedEncoding, { from: beneficiary });

                    console.log('tokenA.transferAndCall():\t', receipt.tx);
                    
                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal(input.amount[0].add(input.amount[0]));
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal('0');
                (await nodeA.methods['earnedTokens()']()).should.be.bignumber.equal(input.feeInToken[0].add(input.feeInToken[0]));
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA.sub(input.amount[0]).sub(input.feeInToken[0]).sub(input.amount[0]).sub(input.feeInToken[0]));
                
                {
                    const id = '0x6ea622eae6590570ec3fd954d787638ea6b2ea0c7dd939dcdd04d8b824ff911f';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);            
                
                    const receipt = await tokenX.methods['transferAndCall(address,uint256,bytes)'](
                        nodeX.address, input.feeInToken[1], signedEncoding, { from: beneficiary });

                    console.log('tokenX.transferAndCall():\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeX, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedTokens()']()).should.be.bignumber.equal(input.feeInToken[1]);
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(input.amount[1].add(input.amount[1]).sub(input.feeInToken[1]));
            });
        });

        describe('Plain vanilla verbose', function () {

            beforeEach(async () => {
                initialAmountTokenA = n('1974');
                await tokenA.methods['transfer(address,uint256)'](beneficiary, initialAmountTokenA);
                input = {
                    network: [domainA, domainX],
                    beneficiary: [beneficiary, beneficiary],
                    token: [tokenA.address, tokenX.address],
                    amount: [n('255.89'), n('255.89')],
                    feeInCash: [n('0.0000044'), n('0.0000088')],
                    feeInToken: [n('1.20'), n('0.65')],
                    operation: ['freeze', 'mint']
                }
            });

            it('deposit | payout', async function () {

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA);
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal('0');

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });
                    
                    const receipt = await nodeA.methods['deposit(bytes32,bytes32[2],address[2],address[2],uint256[2],uint256[2],uint256[2],bytes32[2],bool,uint32,bytes)'](
                        id, signedData.network, signedData.beneficiary, signedData.token, signedData.amount, signedData.feeInCash, signedData.feeInToken, signedData.operation, signedData.fulfilled, signedData.timeout, signedData.signature, { value: input.feeInCash[0], from: beneficiary });

                    console.log('nodeA.deposit():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal(input.amount[0]);
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[0]);
                (await nodeA.methods['earnedTokens()']()).should.be.bignumber.equal('0');
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA.sub(input.amount[0]));
                
                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    const receipt = await nodeX.methods['payout(bytes32,bytes32[2],address[2],address[2],uint256[2],uint256[2],uint256[2],bytes32[2])'](
                        id, data.network, data.beneficiary, data.token, data.amount, data.feeInCash, data.feeInToken, data.operation, { from: router });

                    console.log('nodeX.payout():\t\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeX, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedTokens()']()).should.be.bignumber.equal('0');
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(input.amount[1]);
            });

            it('deposit | withdraw', async function () {
                
                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA);
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal('0');

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });
                    
                    const receipt = await nodeA.methods['deposit(bytes32,bytes32[2],address[2],address[2],uint256[2],uint256[2],uint256[2],bytes32[2],bool,uint32,bytes)'](
                        id, signedData.network, signedData.beneficiary, signedData.token, signedData.amount, signedData.feeInCash, signedData.feeInToken, signedData.operation, signedData.fulfilled, signedData.timeout, signedData.signature, { value: input.feeInCash[0], from: beneficiary });

                    console.log('nodeA.deposit():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal(input.amount[0]);
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[0]);
                (await nodeA.methods['earnedTokens()']()).should.be.bignumber.equal('0');
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA.sub(input.amount[0]));

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    const receipt = await nodeX.methods['withdraw(bytes32,bytes32[2],address[2],address[2],uint256[2],uint256[2],uint256[2],bytes32[2],bool,uint32,bytes)'](
                        id, signedData.network, signedData.beneficiary, signedData.token, signedData.amount, signedData.feeInCash, signedData.feeInToken, signedData.operation, signedData.fulfilled, signedData.timeout, signedData.signature, { value: input.feeInCash[1], from: beneficiary });

                    console.log('nodeX.withdraw():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeX, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[1]);
                (await nodeX.methods['earnedTokens()']()).should.be.bignumber.equal('0');
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(input.amount[1]);
            });
        });

        describe('Plain vanilla with ERC20', function () {

            beforeEach(async () => {
                token1 = await ExenoToken_L1.new(owner);
                token2 = await ExenoToken_L1.new(owner);
                initialAmountTokenA = n('1974');
                initialAmountToken1 = n('20000');
                initialAmountToken2 = n('25000');
                await tokenA.methods['transfer(address,uint256)'](beneficiary, initialAmountTokenA);
                await token1.methods['transfer(address,uint256)'](beneficiary, initialAmountToken1);
                await token2.methods['transfer(address,uint256)'](nodeB.address, initialAmountToken2);
                input = {
                    network: [domainA, domainB],
                    beneficiary: [beneficiary, beneficiary],
                    token: [token1.address, token2.address],
                    amount: [n('13999.69'), n('13999.69')],
                    feeInCash: [n('0.0000123'), n('0.00000456')],
                    feeInToken: [n('12.33'), n('8.12')],
                    operation: ['accept', 'release']
                }
            });

            it('deposit | payout', async function () {

                (await token1.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountToken1);
                (await token2.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal('0');
                (await nodeA.methods['availableTokens(address)'](token1.address)).should.be.bignumber.equal('0');
                (await nodeB.methods['availableTokens(address)'](token2.address)).should.be.bignumber.equal(initialAmountToken2);

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });
                    
                    const receipt = await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.feeInCash[0], from: beneficiary });

                    console.log('nodeA.deposit():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await token1.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountToken1.sub(input.amount[0]));
                (await nodeA.methods['availableTokens(address)'](token1.address)).should.be.bignumber.equal(input.amount[0]);
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[0]);
                
                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    const receipt = await nodeB.methods['payout(bytes)'](
                        encoding, { from: router });

                    console.log('nodeB.payout():\t\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeB, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await token2.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(input.amount[1]);
                (await nodeB.methods['availableTokens(address)'](token2.address)).should.be.bignumber.equal(initialAmountToken2.sub(input.amount[1]));
                (await nodeB.methods['earnedCash()']()).should.be.bignumber.equal('0');
            });

            it('deposit | withdraw', async function () {

                (await token1.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountToken1);
                (await token2.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal('0');
                (await nodeA.methods['availableTokens(address)'](token1.address)).should.be.bignumber.equal('0');
                (await nodeB.methods['availableTokens(address)'](token2.address)).should.be.bignumber.equal(initialAmountToken2);

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });
                    
                    const receipt = await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.feeInCash[0], from: beneficiary });

                    console.log('nodeA.deposit():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await token1.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountToken1.sub(input.amount[0]));
                (await nodeA.methods['availableTokens(address)'](token1.address)).should.be.bignumber.equal(input.amount[0]);
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[0]);
                
                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    const receipt = await nodeB.methods['withdraw(bytes)'](
                        signedEncoding, { value: input.feeInCash[1], from: beneficiary });

                    console.log('nodeB.withdraw():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeB, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeB.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[1]);
                (await token2.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(input.amount[1]);
                (await nodeB.methods['availableTokens(address)'](token2.address)).should.be.bignumber.equal(initialAmountToken2.sub(input.amount[1]));
            });

            it('transfer | payout', async function () {

                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA);
                (await token1.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountToken1);
                (await token2.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal('0');
                (await nodeA.methods['availableTokens(address)'](token1.address)).should.be.bignumber.equal('0');
                (await nodeB.methods['availableTokens(address)'](token2.address)).should.be.bignumber.equal(initialAmountToken2);

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    const receipt = await tokenA.methods['transferAndCall(address,uint256,bytes)'](
                        nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary });

                    console.log('tokenA.transferAndCall():\t', receipt.tx);
                    
                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA.sub(input.feeInToken[0]));
                (await token1.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountToken1.sub(input.amount[0]));
                (await nodeA.methods['availableTokens(address)'](token1.address)).should.be.bignumber.equal(input.amount[0]);
                (await nodeA.methods['earnedTokens()']()).should.be.bignumber.equal(input.feeInToken[0]);
                
                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    const receipt = await nodeB.methods['payout(bytes)'](
                        encoding, { from: router });

                    console.log('nodeB.payout():\t\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeB, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await token2.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(input.amount[1]);
                (await nodeB.methods['availableTokens(address)'](token2.address)).should.be.bignumber.equal(initialAmountToken2.sub(input.amount[1]));
                (await nodeB.methods['earnedCash()']()).should.be.bignumber.equal('0');
            });
        });

        describe('Plain vanilla with cash', function () {

            beforeEach(async () => {
                initialAmountCash = n('0.0050');
                await nodeX.sendTransaction({ value: initialAmountCash });
                input = {
                    network: [domainA, domainX],
                    beneficiary: [beneficiary, beneficiary],
                    token: [constants.ZERO_ADDRESS, constants.ZERO_ADDRESS],
                    amount: [n('0.0011'), n('0.0022')],
                    feeInCash: [n('0.0000033'), n('0.0000055')],
                    feeInToken: [n('2.10'), n('4.99')],
                    operation: ['accept', 'release']
                }
            });

            it('deposit | payout', async function () {

                (await nodeA.methods['availableCash()']()).should.be.bignumber.equal('0');
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['availableCash()']()).should.be.bignumber.equal(initialAmountCash);
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal('0');

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);
                    
                    const receipt = await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.amount[0].add(input.feeInCash[0]), from: beneficiary });

                    console.log('nodeA.deposit():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeA.methods['availableCash()']()).should.be.bignumber.equal(input.amount[0].add(input.feeInCash[0]));
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[0]);
                
                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    const receipt = await nodeX.methods['payout(bytes)'](
                        encoding, { from: router });

                    console.log('nodeX.payout():\t\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeX, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeX.methods['availableCash()']()).should.be.bignumber.equal(initialAmountCash.sub(input.amount[1]));
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal('0');
            });

            it('deposit | withdraw', async function () {

                (await nodeA.methods['availableCash()']()).should.be.bignumber.equal('0');
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['availableCash()']()).should.be.bignumber.equal(initialAmountCash);
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal('0');

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);
                    
                    const receipt = await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.amount[0].add(input.feeInCash[0]), from: beneficiary });

                    console.log('nodeA.deposit():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeA.methods['availableCash()']()).should.be.bignumber.equal(input.amount[0].add(input.feeInCash[0]));
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[0]);
                
                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    const receipt = await nodeX.methods['withdraw(bytes)'](
                        signedEncoding, { value: input.feeInCash[1], from: beneficiary });

                    console.log('nodeX.withdraw():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeX, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeX.methods['availableCash()']()).should.be.bignumber.equal(initialAmountCash.sub(input.amount[1]).add(input.feeInCash[1]));
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[1]);
            });
        });

        describe('Entire loop', function () {

            beforeEach(async () => {
                initialAmountTokenA = n('1974');
                await tokenA.methods['transfer(address,uint256)'](beneficiary, initialAmountTokenA);
            });

            it('transfer | withdraw | deposit | transfer', async function () {

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA);
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal('0');

                input = {
                    network: [domainA, domainX],
                    beneficiary: [beneficiary, beneficiary],
                    token: [tokenA.address, tokenX.address],
                    amount: [n('255.89'), n('255.89')],
                    feeInCash: [n('0.0000044'), n('0.0000088')],
                    feeInToken: [n('1.20'), n('0.65')],
                    operation: ['freeze', 'mint']
                }

                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    const receipt = await tokenA.methods['transferAndCall(address,uint256,bytes)'](
                        nodeA.address, input.amount[0].add(input.feeInToken[0]), signedEncoding, { from: beneficiary });

                    console.log('tokenA.transferAndCall():\t', receipt.tx);
                    
                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal(input.amount[0]);
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal('0');
                (await nodeA.methods['earnedTokens()']()).should.be.bignumber.equal(input.feeInToken[0]);
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA.sub(input.amount[0]).sub(input.feeInToken[0]));
                
                {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);            
                    
                    const receipt = await nodeX.methods['withdraw(bytes)'](
                        signedEncoding, { value: input.feeInCash[1], from: beneficiary });

                    console.log('nodeX.withdraw():\t\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeX, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[1]);
                (await nodeX.methods['earnedTokens()']()).should.be.bignumber.equal('0');
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(input.amount[1]);

                input = {
                    network: [domainX, domainA],
                    beneficiary: [beneficiary, beneficiary],
                    token: [tokenX.address, tokenA.address],
                    amount: [n('255.89'), n('255.89')],
                    feeInCash: [n('0.0000088'), n('0.0000044')],
                    feeInToken: [n('0.65'), n('1.20')],
                    operation: ['burn', 'unfreeze']
                }
                
                {
                    const id = '0x6ea622eae6590570ec3fd954d787638ea6b2ea0c7dd939dcdd04d8b824ff911f';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenX.methods['approve(address,uint256)'](
                        nodeX.address, input.amount[0], { from: beneficiary });
                    
                    const receipt = await nodeX.methods['deposit(bytes)'](
                        signedEncoding, { value: input.feeInCash[0], from: beneficiary });

                    console.log('nodeX.deposit():\t\t', receipt.tx);
                    
                    await expectEvent.inTransaction(receipt.tx, nodeX, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeX.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeX.methods['earnedCash()']()).should.be.bignumber.equal(input.feeInCash[0].add(input.feeInCash[0]));
                (await nodeX.methods['earnedTokens()']()).should.be.bignumber.equal('0');
                (await tokenX.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal('0');
                
                {
                    const id = '0x6ea622eae6590570ec3fd954d787638ea6b2ea0c7dd939dcdd04d8b824ff911f';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);            
                
                    const receipt = await tokenA.methods['transferAndCall(address,uint256,bytes)'](
                        nodeA.address, input.feeInToken[1], signedEncoding, { from: beneficiary });

                    console.log('tokenA.transferAndCall():\t', receipt.tx);

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Withdraw', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                }

                (await nodeA.methods['frozenTokens()']()).should.be.bignumber.equal('0');
                (await nodeA.methods['earnedCash()']()).should.be.bignumber.equal('0');
                (await nodeA.methods['earnedTokens()']()).should.be.bignumber.equal(input.feeInToken[1].add(input.feeInToken[1]));
                (await tokenA.methods['balanceOf(address)'](beneficiary)).should.be.bignumber.equal(initialAmountTokenA.sub(input.feeInToken[1]).sub(input.feeInToken[1]));
            });

        });
    });

    describe('Deposits', function () {
        describe('Core non-mintable deposit..', function () {
            beforeEach(async () => {
                initialAmountTokenA = n('1974');
                await tokenA.methods['transfer(address,uint256)'](beneficiary, initialAmountTokenA);
                await tokenA.methods['transfer(address,uint256)'](beneficiary2, initialAmountTokenA);
                input = {
                    network: [domainA, domainX],
                    beneficiary: [beneficiary, beneficiary],
                    token: [tokenA.address, tokenX.address],
                    amount: [n('255.89'), n('255.89')],
                    feeInCash: [n('0.0000044'), n('0.0000088')],
                    feeInToken: [n('1.20'), n('0.65')],
                    operation: ['freeze', 'mint']
                }
                block = await getBlock();
            });

            describe('is accepted when..', function () {
                it('correct arguments are supplied', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    const receipt = await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.feeInCash[0], from: beneficiary });

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                });
            });

            describe('is rejected when..', function () {
                it('signature is wrong', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0, "e6f4ecc4587ef0bf4383b1468b04a82f3b1bd9422e6d781cd7488cc916b5ab56");

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: unauthorized signer"
                    );
                });

                it('there is a payload type mismatch', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: this payload is not a deposit"
                    );
                });

                it('there is a sender address mismatch', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary2 });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary2 }),
                        "ExenoBridgeNode: unexpected sender"
                    );
                });

                it('not enough cash is attached to cover fee', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: not enough cash attached to cover fee"
                    );

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0].sub(n('0.0000001')), from: beneficiary }),
                        "ExenoBridgeNode: not enough cash attached to cover fee"
                    );
                });

                it('user has not approved the appropriate amount', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: before depositing user must approve amount"
                    );
                    
                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0].sub(n('0.1')), { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: before depositing user must approve amount"
                    );
                });

                it('timeout has already expired', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, block.timestamp + period.hours(24));

                    await advanceTimeAndBlock(period.hours(25));

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: time-window for deposit has already expired"
                    );
                });

                it('the same ID is used more than once', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });
                    
                    await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.feeInCash[0], from: beneficiary });

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: deposit already processed"
                    );
                });

                it('there is an operation mismatch', async function () {
                    input.operation[0] = 'burn';
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: unexpected deposit operation"
                    );
                });

                it('contract is paused', async function () {
                    await nodeA.methods['pause()']();

                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "Pausable: paused"
                    );
                });
            });
        });

        describe('Core mintable deposit..', function () {
            beforeEach(async () => {
                initialAmountTokenX = n('1974');
                await tokenX.methods['setManager(address)'](owner);
                await tokenX.methods['mint(address,uint256)'](beneficiary, initialAmountTokenX);
                await tokenX.methods['mint(address,uint256)'](beneficiary2, initialAmountTokenX);
                await tokenX.methods['setManager(address)'](nodeX.address);
                input = {
                    network: [domainX, domainA],
                    beneficiary: [beneficiary, beneficiary],
                    token: [tokenX.address, tokenA.address],
                    amount: [n('255.89'), n('255.89')],
                    feeInCash: [n('0.0000044'), n('0.0000088')],
                    feeInToken: [n('1.20'), n('0.65')],
                    operation: ['burn', 'unfreeze']
                }
                block = await getBlock();
            });

            describe('is accepted when..', function () {
                it('correct arguments are supplied', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenX.methods['approve(address,uint256)'](
                        nodeX.address, input.amount[0], { from: beneficiary });

                    const receipt = await nodeX.methods['deposit(bytes)'](
                        signedEncoding, { value: input.feeInCash[0], from: beneficiary });

                    await expectEvent.inTransaction(receipt.tx, nodeX, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                });
            });

            describe('is rejected when..', function () {
                it('signature is wrong', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0, "e6f4ecc4587ef0bf4383b1468b04a82f3b1bd9422e6d781cd7488cc916b5ab56");

                    await tokenX.methods['approve(address,uint256)'](
                        nodeX.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeX.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: unauthorized signer"
                    );
                });

                it('there is a payload type mismatch', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    await tokenX.methods['approve(address,uint256)'](
                        nodeX.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeX.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: this payload is not a deposit"
                    );
                });

                it('there is a sender address mismatch', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenX.methods['approve(address,uint256)'](
                        nodeX.address, input.amount[0], { from: beneficiary2 });

                    await expectRevert(
                        nodeX.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary2 }),
                        "ExenoBridgeNode: unexpected sender"
                    );
                });

                it('not enough cash is attached to cover fee', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenX.methods['approve(address,uint256)'](
                        nodeX.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeX.methods['deposit(bytes)'](signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: not enough cash attached to cover fee"
                    );

                    await expectRevert(
                        nodeX.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0].sub(n('0.0000001')), from: beneficiary }),
                        "ExenoBridgeNode: not enough cash attached to cover fee"
                    );
                });

                it('user has not approved the appropriate amount', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await expectRevert(
                        nodeX.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: before depositing user must approve amount"
                    );
                    
                    await tokenX.methods['approve(address,uint256)'](
                        nodeX.address, input.amount[0].sub(n('0.1')), { from: beneficiary });

                    await expectRevert(
                        nodeX.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: before depositing user must approve amount"
                    );
                });

                it('timeout has already expired', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, block.timestamp + period.hours(24));

                    await advanceTimeAndBlock(period.hours(25));

                    await tokenX.methods['approve(address,uint256)'](
                        nodeX.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeX.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: time-window for deposit has already expired"
                    );
                });

                it('the same ID is used more than once', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenX.methods['approve(address,uint256)'](
                        nodeX.address, input.amount[0], { from: beneficiary });
                    
                    await nodeX.methods['deposit(bytes)'](
                        signedEncoding, { value: input.feeInCash[0], from: beneficiary });

                    await tokenX.methods['approve(address,uint256)'](
                        nodeX.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeX.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: deposit already processed"
                    );
                });

                it('burn operation is not applied to a mintable token', async function () {
                    input.operation[0] = 'freeze';
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenX.methods['approve(address,uint256)'](
                        nodeX.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeX.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: burn operation is expected for a mintable token"
                    );
                });
            });
        });

        describe('Non-core deposit..', function () {
            beforeEach(async () => {
                token1 = await ExenoToken_L1.new(owner);
                token2 = await ExenoToken_L1.new(owner);
                initialAmountToken1 = n('20000');
                initialAmountToken2 = n('25000');
                await token1.methods['transfer(address,uint256)'](beneficiary, initialAmountToken1);
                await token2.methods['transfer(address,uint256)'](nodeB.address, initialAmountToken2);
                input = {
                    network: [domainA, domainB],
                    beneficiary: [beneficiary, beneficiary],
                    token: [token1.address, token2.address],
                    amount: [n('13999.69'), n('13999.69')],
                    feeInCash: [n('0.0000123'), n('0.00000456')],
                    feeInToken: [n('12.33'), n('8.12')],
                    operation: ['accept', 'release']
                }
                block = await getBlock();
            });

            describe('is accepted when..', function () {
                it('correct arguments are supplied', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    const receipt = await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.feeInCash[0], from: beneficiary });

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                });
            });

            describe('is rejected when..', function () {
                it('signature is wrong', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0, "e6f4ecc4587ef0bf4383b1468b04a82f3b1bd9422e6d781cd7488cc916b5ab56");

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: unauthorized signer"
                    );
                });
                
                it('there is a payload type mismatch', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: this payload is not a deposit"
                    );
                });

                it('there is a sender address mismatch', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary2 });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary2 }),
                        "ExenoBridgeNode: unexpected sender"
                    );
                });

                it('not enough cash is attached to cover fee', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: not enough cash attached to cover fee"
                    );

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0].sub(n('0.0000001')), from: beneficiary }),
                        "ExenoBridgeNode: not enough cash attached to cover fee"
                    );
                });

                it('user has not approved the appropriate amount', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: before depositing user must approve amount"
                    );
                    
                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0].sub(n('0.1')), { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: before depositing user must approve amount"
                    );
                });

                it('timeout has already expired', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, block.timestamp + period.hours(24));

                    await advanceTimeAndBlock(period.hours(25));

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: time-window for deposit has already expired"
                    );
                });

                it('the same ID is used more than once', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });
                    
                    await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.feeInCash[0], from: beneficiary });

                    await token1.methods['transfer(address,uint256)'](beneficiary, initialAmountToken1);
                    await token2.methods['transfer(address,uint256)'](nodeB.address, initialAmountToken2);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: deposit already processed"
                    );
                });

                it('freeze operation is applied to a non-core token', async function () {
                    input.operation[0] = 'freeze';
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: freeze operation cannot be done on a non-core token"
                    );
                });

                it('there is an operation mismatch', async function () {
                    input.operation[0] = 'burn';
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.feeInCash[0], from: beneficiary }),
                        "ExenoBridgeNode: unexpected deposit operation"
                    );
                });
            });
        });

        describe('Cash deposit..', function () {
            beforeEach(async () => {
                initialAmountCash = n('0.0050');
                await nodeX.sendTransaction({ value: initialAmountCash });
                input = {
                    network: [domainA, domainX],
                    beneficiary: [beneficiary, beneficiary],
                    token: [constants.ZERO_ADDRESS, constants.ZERO_ADDRESS],
                    amount: [n('0.0011'), n('0.0022')],
                    feeInCash: [n('0.0000033'), n('0.0000055')],
                    feeInToken: [n('2.10'), n('4.99')],
                    operation: ['accept', 'release']
                }
                block = await getBlock();
            });

            describe('is accepted when..', function () {
                it('correct arguments are supplied', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    const receipt = await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.amount[0].add(input.feeInCash[0]), from: beneficiary });

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                });
            });

            describe('is rejected when..', function () {
                it('signature is wrong', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0, "e6f4ecc4587ef0bf4383b1468b04a82f3b1bd9422e6d781cd7488cc916b5ab56");

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.amount[0].add(input.feeInCash[0]), from: beneficiary }),
                        "ExenoBridgeNode: unauthorized signer"
                    );
                });
                
                it('there is a payload type mismatch', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, true, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.amount[0].add(input.feeInCash[0]), from: beneficiary }),
                        "ExenoBridgeNode: this payload is not a deposit"
                    );
                });

                it('there is a sender address mismatch', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.amount[0].add(input.feeInCash[0]), from: beneficiary2 }),
                        "ExenoBridgeNode: unexpected sender"
                    );
                });

                it('not enough cash is attached to cover amount and fee', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: not enough cash attached to cover amount and fee"
                    );

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.amount[0].add(input.feeInCash[0]).sub(n('0.0000001')), from: beneficiary }),
                        "ExenoBridgeNode: not enough cash attached to cover amount and fee"
                    );
                });

                it('timeout has already expired', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, block.timestamp + period.hours(24));

                    await advanceTimeAndBlock(period.hours(25));

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.amount[0].add(input.feeInCash[0]), from: beneficiary }),
                        "ExenoBridgeNode: time-window for deposit has already expired"
                    );
                });

                it('the same ID is used more than once', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await nodeA.methods['deposit(bytes)'](
                        signedEncoding, { value: input.amount[0].add(input.feeInCash[0]), from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.amount[0].add(input.feeInCash[0]), from: beneficiary }),
                        "ExenoBridgeNode: deposit already processed"
                    );
                });

                it('freeze operation is applied to a non-core token', async function () {
                    input.operation[0] = 'freeze';
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.amount[0].add(input.feeInCash[0]), from: beneficiary }),
                        "ExenoBridgeNode: freeze operation cannot be done on a non-core token"
                    );
                });

                it('there is an operation mismatch', async function () {
                    input.operation[0] = 'burn';
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await tokenA.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        nodeA.methods['deposit(bytes)'](signedEncoding, { value: input.amount[0].add(input.feeInCash[0]), from: beneficiary }),
                        "ExenoBridgeNode: unexpected deposit operation"
                    );
                });
            });
        });

        describe('T&C core deposit..', function () {
            beforeEach(async () => {
                initialAmountTokenA = n('1974');
                await tokenA.methods['transfer(address,uint256)'](beneficiary, initialAmountTokenA);
                await tokenA.methods['transfer(address,uint256)'](beneficiary2, initialAmountTokenA);
                input = {
                    network: [domainA, domainX],
                    beneficiary: [beneficiary, beneficiary],
                    token: [tokenA.address, tokenX.address],
                    amount: [n('255.89'), n('255.89')],
                    feeInCash: [n('0.0000044'), n('0.0000088')],
                    feeInToken: [n('1.20'), n('0.65')],
                    operation: ['freeze', 'mint']
                }
                block = await getBlock();
            });

            describe('is accepted when..', function () {
                it('correct arguments are supplied', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    const receipt = await tokenA.methods['transferAndCall(address,uint256,bytes)'](
                        nodeA.address, input.amount[0].add(input.feeInToken[0]), signedEncoding, { from: beneficiary });
                    
                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                });
            });

            describe('is rejected when..', function () {
                it('signature is wrong', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0, "e6f4ecc4587ef0bf4383b1468b04a82f3b1bd9422e6d781cd7488cc916b5ab56");

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.amount[0].add(input.feeInToken[0]), signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: unauthorized signer"
                    );
                });
                
                it('there is a sender address mismatch', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.amount[0].add(input.feeInToken[0]), signedEncoding, { from: beneficiary2 }),
                        "ExenoBridgeNode: unexpected sender"
                    );
                });

                it('not enough core tokens are transferred to cover amount and fee', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.amount[0].add(input.feeInToken[0]).sub(n('0.1')), signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: not enough core tokens transferred to cover amount and fee"
                    );
                });

                it('timeout has already expired', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, block.timestamp + period.hours(24));

                    await advanceTimeAndBlock(period.hours(25));

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.amount[0].add(input.feeInToken[0]), signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: time-window for deposit has already expired"
                    );
                });

                it('the same ID is used more than once', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);
                    
                    await tokenA.methods['transferAndCall(address,uint256,bytes)'](
                        nodeA.address, input.amount[0].add(input.feeInToken[0]), signedEncoding, { from: beneficiary });

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.amount[0].add(input.feeInToken[0]), signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: deposit already processed"
                    );
                });

                it('there is an operation mismatch', async function () {
                    input.operation[0] = 'burn';
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.amount[0].add(input.feeInToken[0]), signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: unexpected deposit operation"
                    );
                });
            });
        });

        describe('T&C non-core deposit..', function () {
            beforeEach(async () => {
                token1 = await ExenoToken_L1.new(owner);
                token2 = await ExenoToken_L1.new(owner);
                initialAmountTokenA = n('1974');
                initialAmountToken1 = n('20000');
                await tokenA.methods['transfer(address,uint256)'](beneficiary, initialAmountTokenA);
                await tokenA.methods['transfer(address,uint256)'](beneficiary2, initialAmountTokenA);
                await token1.methods['transfer(address,uint256)'](beneficiary, initialAmountToken1);
                await token1.methods['transfer(address,uint256)'](beneficiary2, initialAmountToken1);
                input = {
                    network: [domainA, domainB],
                    beneficiary: [beneficiary, beneficiary],
                    token: [token1.address, token2.address],
                    amount: [n('13999.69'), n('13999.69')],
                    feeInCash: [n('0.0000123'), n('0.00000456')],
                    feeInToken: [n('12.33'), n('8.12')],
                    operation: ['accept', 'release']
                }
                block = await getBlock();
            });

            describe('is accepted when..', function () {
                it('correct arguments are supplied', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    const receipt = await tokenA.methods['transferAndCall(address,uint256,bytes)'](
                        nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary });

                    await expectEvent.inTransaction(receipt.tx, nodeA, 'Deposit', {
                        id,
                        network: data.network,
                        beneficiary: data.beneficiary,
                        token: data.token,
                        amount: data.amount,
                        feeInCash: data.feeInCash,
                        feeInToken: data.feeInToken,
                        operation: data.operation
                    });
                });
            });

            describe('is rejected when..', function () {
                it('signature is wrong', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0, "e6f4ecc4587ef0bf4383b1468b04a82f3b1bd9422e6d781cd7488cc916b5ab56");

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: unauthorized signer"
                    );
                });

                it('there is a sender address mismatch', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary2 });

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary2 }),
                        "ExenoBridgeNode: unexpected sender"
                    );
                });

                it('token fee is zero', async function () {
                    input.feeInToken[0] = n('0');
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: fee needs to be non-zero"
                    );
                });

                it('user has not approved the appropriate amount', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: before depositing user must approve amount"
                    );

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0].sub(n('0.1')), { from: beneficiary });

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: before depositing user must approve amount"
                    );
                });

                it('not enough core tokens are transferred to cover fee', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.feeInToken[0].sub(n('0.1')), signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: not enough core tokens transferred to cover fee"
                    );
                });

                it('timeout has already expired', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, block.timestamp + period.hours(24));

                    await advanceTimeAndBlock(period.hours(25));

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: time-window for deposit has already expired"
                    );
                });

                it('the same ID is used more than once', async function () {
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });
                    
                    await tokenA.methods['transferAndCall(address,uint256,bytes)'](
                        nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary });

                    await tokenA.methods['transfer(address,uint256)'](beneficiary, initialAmountTokenA);
                    await token1.methods['transfer(address,uint256)'](beneficiary, initialAmountToken1);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: deposit already processed"
                    );
                });

                it('freeze operation is applied to a non-core token', async function () {
                    input.operation[0] = 'freeze';
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: freeze operation cannot be done on a non-core token"
                    );
                });

                it('there is an operation mismatch', async function () {
                    input.operation[0] = 'burn';
                    const id = '0x7c0185fccaaeb128fb3b59498354150381faba3193fe0d29fdb9a7a533e1a2f0';
                    const { data, encoding, signedData, signedEncoding } = sign(input, id, false, 0);

                    await token1.methods['approve(address,uint256)'](
                        nodeA.address, input.amount[0], { from: beneficiary });

                    await expectRevert(
                        tokenA.methods['transferAndCall(address,uint256,bytes)'](nodeA.address, input.feeInToken[0], signedEncoding, { from: beneficiary }),
                        "ExenoBridgeNode: unexpected deposit operation"
                    );
                });
            });
        });
    });
});
