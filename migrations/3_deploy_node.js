const ExenoToken_L1 = artifacts.require("ExenoToken_L1");
const ExenoToken_L2 = artifacts.require("ExenoToken_L2");
const ExenoBridgeNode = artifacts.require("ExenoBridgeNode");

module.exports = async function (deployer, network, accounts) {

    await deployer.deploy(
        ExenoBridgeNode,
        ExenoToken_L1.address,
        accounts[1],
        accounts[2]
    );

    await deployer.deploy(
        ExenoBridgeNode,
        ExenoToken_L2.address,
        accounts[1],
        accounts[2]
    );

    return true;
}
