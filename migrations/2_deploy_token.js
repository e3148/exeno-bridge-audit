const ExenoToken_L1 = artifacts.require("ExenoToken_L1");
const ExenoToken_L2 = artifacts.require("ExenoToken_L2");

module.exports = async function (deployer, network, accounts) {
    await deployer.deploy(ExenoToken_L1, accounts[0]);
    await deployer.deploy(ExenoToken_L2);
    return true;
}
